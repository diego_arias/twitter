
# Twitter R

In this document I'll show how to extract _twits_ with **R**, using de _Twitter Api_

# Create account in Twitter API

Steps:
 - Go to the link: https://developer.twitter.com/en/apps
 - Sign in with a Twitter Account
 - Click on **Create new app**
 - Write the information required for the new application
 - Agree the _Developer Agreement_
 - Click on **Create your twitter application**
 - In the new page shown, click on **Keys and Access Tokens**
 - Scroll down and click on **Create My Access Token**
 - Now you can see you _Acees Token Key_ for the _Twitter API_

# Install twitterR

We need to install the package to scrap twits from R


```R
install.packages("twitteR")
```

    Warning message:
    “package ‘twitterR’ is not available (for R version 3.5.1)”

# Keys

Declare all the neccesary keys and load the library twitterR


```R
library(twitteR)
consumer_key    <- 'EjdFzDBaZLRQYM0GlFGzw86ED'# API KEY 
consumer_secret <- 'p8o9rLoAsljVCkhHF1XgoxTo1ZiiLAhXMb9bI6odJhw73yPUz5' # API secret key
access_token    <- '225533601-vTUG3dHt6fzwwO2prU1pqw6PGUjeRk3LDL2D1Pck' # Access token
access_secret   <- 'TdvBUXctvVYCJxo5QoV7J5jE0zPgaMiMFSP6h2RTU7CHa' # Access token secret
```

# Connection with Twitter

Set a connection with Twitter with the keys


```R
setup_twitter_oauth(consumer_key, consumer_secret, access_token, access_secret)
```

    [1] "Using direct authentication"


# Search hashtags


```R
tw = twitteR::searchTwitter('@realDonaldTrump', # Hashtag to search
                            n = 500, # Number of twits to extract
                            since = '2019-01-01',
                            retryOnRateLimit = 300,
                            )
```

Save _twits_ text


```R
twits_text <- sapply(tw, function(x) x$getText())
```


```R
head(twits_text)
```


<ol class=list-inline>
	<li>'RT @realDonaldTrump: Joe Biden got tongue tied over the weekend when he was unable to properly deliver a very simple line about his decisio…'</li>
	<li><span style=white-space:pre-wrap>'RT @CheriJacobus: @realDonaldTrump What\'s the other side of lying, treasonous, racist, misogynistic, cheating, stealing, incompetence?  If…'</span></li>
	<li>'@realDonaldTrump @SpeakerPelosi @HouseDemocrats @SenateDems TRUMP - U R NOT FIT 2 BE PREZ. U PRAISE DICTATORS BUT N… https://t.co/7NMOuQIOfD'</li>
	<li>'RT @realDonaldTrump: While the press doesn’t like writing about it, nor do I need them to, I donate my yearly Presidential salary of $400,0…'</li>
	<li>'@gtconway3d @realDonaldTrump Americans For ❤️ Integrity Of Truth \nKnew This Of Trump Years Ago \nWhat Took So Long G… https://t.co/S6UC0wreyY'</li>
	<li>'@realDonaldTrump https://t.co/jMXAoUUjHw'</li>
</ol>



# Cleaning twits

We only want _twits_ that starts with the Donald Trump's twitter account, so we need all _twits_ that starts with _@realDonaldTrump_


```R
library(dplyr)
```


```R
twits_text %>%
 data.frame %>%
 filter(substr(.,1,nchar('@realDonaldTrump'))=='@realDonaldTrump') %>%
 pull %>%
 as.vector() -> twits_text
```


```R
head(twits_text,20)
```


<ol class=list-inline>
	<li>'@realDonaldTrump @SpeakerPelosi @HouseDemocrats @SenateDems TRUMP - U R NOT FIT 2 BE PREZ. U PRAISE DICTATORS BUT N… https://t.co/7NMOuQIOfD'</li>
	<li>'@realDonaldTrump https://t.co/jMXAoUUjHw'</li>
	<li>'@realDonaldTrump Thank you President Trump !'</li>
	<li>'@realDonaldTrump https://t.co/Uj1Hr4i4Kw'</li>
	<li>'@realDonaldTrump That\'s just like you... Destroy the foundation you were built on...'</li>
	<li>'@realDonaldTrump @FLOTUS #BeBest'</li>
	<li>'@realDonaldTrump #insane #25thAmendment'</li>
	<li>'@realDonaldTrump Oh you think Hispanics love you do you? I hadn\'t felt like a victim of racism until your campaign.… https://t.co/nTTTEOVZ4P'</li>
	<li>'@realDonaldTrump @Varneyco Wow! Gallup poll: President Trump’s job approval on the economy reaches NEW HIGH! https://t.co/oZ5yMpnRFi'</li>
	<li>'@realDonaldTrump Mr. President, please have your staff look into this TSA “agent.” TSA is out of control!\nThank you… https://t.co/HTrpTxrGd2'</li>
	<li>'@realDonaldTrump Keep on selling that no salary line as you have made millions extra via Trump properties using the… https://t.co/uBSfhtR51G'</li>
	<li>'@realDonaldTrump This isn\'t worth your time'</li>
	<li>'@realDonaldTrump Not 100% DONNY? Even some of your own don\'t approve...'</li>
	<li>'@realDonaldTrump #LiarInChief \U0001f925'</li>
	<li>'@realDonaldTrump Look what I found on Facebook at "worst President ever": \U0001f923 https://t.co/4LH8ITIU1C'</li>
	<li>'@realDonaldTrump https://t.co/mq3pvqoFF9'</li>
	<li>'@realDonaldTrump This is what you should be looking into! https://t.co/bKeAPxhKeh'</li>
	<li>'@realDonaldTrump All America is behind you Mr. President. God bless President Trump and God bless America.'</li>
	<li>'@realDonaldTrump DJT works at FB Helpdesk!'</li>
	<li>'@realDonaldTrump https://t.co/cGbhJoX5Ri'</li>
</ol>


